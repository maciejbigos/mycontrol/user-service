package pl.bigosmaciej.mycontrol.userservice.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.bigosmaciej.mycontrol.userservice.model.dto.UpdateUserDataDto;
import pl.bigosmaciej.mycontrol.userservice.model.pojo.User;
import pl.bigosmaciej.mycontrol.userservice.service.UserService;

import java.util.Optional;

@Slf4j
@RestController
@AllArgsConstructor
public class UserController {

    public static final String HEADER_NAME = "X-User-Id-Y";
    private final UserService userService;

    @GetMapping("/get/{id}")
    public ResponseEntity<?> getUser(@PathVariable String id) {
        Optional<User> user = userService.getUserById(id);
        if (user.isEmpty()) {
            return ResponseEntity.badRequest().body("User not found");
        }
        return ResponseEntity.ok(user.get());
    }

    @GetMapping("/me")
    public ResponseEntity<?> getMe(@RequestHeader(HEADER_NAME) String id) {
        Optional<User> user = userService.getUserById(id);
        if (user.isEmpty()) {
            //If ever happens, big trouble
            log.error("User from token not found, id from token {}", id);
            return ResponseEntity.badRequest().body("User not found");
        }
        return ResponseEntity.ok(user.get());
    }

    @PutMapping("/me/update")
    public ResponseEntity<String> updateMe(@RequestHeader(HEADER_NAME) String id, @RequestBody UpdateUserDataDto form) {
        String message = userService.updateUser(form, id);
        return ResponseEntity.ok(message);
    }

    @DeleteMapping("/me")
    public ResponseEntity<?> deleteMe(@RequestHeader(HEADER_NAME) String id) {
        userService.deleteUser(id); //todo implement
        return ResponseEntity.ok("User deleted");
    }


}
