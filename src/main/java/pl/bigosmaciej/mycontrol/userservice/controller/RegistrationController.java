package pl.bigosmaciej.mycontrol.userservice.controller;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.bigosmaciej.mycontrol.userservice.model.dto.CreateUserDto;
import pl.bigosmaciej.mycontrol.userservice.service.UserService;

@RestController
@RequestMapping("/register")
@AllArgsConstructor
public class RegistrationController {

    public static final String USER_CREATED = "User %s created";
    private final UserService registrationService;

    @PostMapping
    public ResponseEntity<String> registerUser(@Valid @RequestBody CreateUserDto userDto) {
        String id =registrationService.registerUser(userDto);
        return ResponseEntity.ok(String.format(USER_CREATED, id));
    }

}
