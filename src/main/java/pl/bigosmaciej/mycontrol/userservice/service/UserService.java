package pl.bigosmaciej.mycontrol.userservice.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.bigosmaciej.mycontrol.userservice.model.dto.CreateUserDto;
import pl.bigosmaciej.mycontrol.userservice.model.dto.UpdateUserDataDto;
import pl.bigosmaciej.mycontrol.userservice.model.entity.UserEntity;
import pl.bigosmaciej.mycontrol.userservice.model.pojo.User;
import pl.bigosmaciej.mycontrol.userservice.model.repository.UserRepository;

import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public String registerUser(CreateUserDto userFrom) {

        UserEntity userEntity = new UserEntity();

        userEntity.setEmail(userFrom.email());
        userEntity.setPassword(passwordEncoder.encode(userFrom.password()));

        userRepository.saveAndFlush(userEntity);

        log.info("User with id {} created", userEntity.getId());

        return userEntity.getId();
    }

    public String updateUser(UpdateUserDataDto form, String id) {
        Optional<UserEntity> opt = userRepository.findById(id);

        if (opt.isEmpty()) {
            log.error("User to update with id {} don't exists", id);
            return "Update failed";
        }

        UserEntity user = opt.get();
        StringBuilder stringBuilder = new StringBuilder();

        if (!form.firstName().isEmpty()) {
            user.setFirstName(form.firstName());
            stringBuilder.append("firstName, ");
        }

        if (!form.lastName().isEmpty()) {
            user.setLastName(form.lastName());
            stringBuilder.append("lastName, ");
        }

        if (!form.displayUsername().isEmpty()) {
            user.setDisplayUsername(form.displayUsername());
            stringBuilder.append("displayName");
        }

        userRepository.save(user);

        return String.format("User data: [%s] updated",stringBuilder);
    }

    public Optional<User> getUserById(String id) {
        Optional<UserEntity> opt = userRepository.findById(id);

        if (opt.isEmpty()) {
            log.warn("User with id {} don't exists", id);
            return Optional.empty();
        }

        UserEntity userEntity = opt.get();

        return Optional.of(new User(
                userEntity.getId(),
                userEntity.getFirstName(),
                userEntity.getFirstName(),
                userEntity.getDisplayUsername(),
                userEntity.getEmail()
        ));
    }

    @Deprecated
    public void deleteUser(String id) {
        userRepository.deleteById(id);
    }



}
