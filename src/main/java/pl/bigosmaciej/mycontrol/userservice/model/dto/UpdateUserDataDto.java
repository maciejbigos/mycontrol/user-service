package pl.bigosmaciej.mycontrol.userservice.model.dto;

public record UpdateUserDataDto(
        String firstName,
        String lastName,
        String displayUsername)
{ }
