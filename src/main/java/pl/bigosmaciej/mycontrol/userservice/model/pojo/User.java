package pl.bigosmaciej.mycontrol.userservice.model.pojo;

public record User(
        String id,
        String firstName,
        String lastName,
        String displayUsername,
        String email
) { }
