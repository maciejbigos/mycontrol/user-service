package pl.bigosmaciej.mycontrol.userservice.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Email;
import pl.bigosmaciej.mycontrol.userservice.model.validator.UniqueEmail;


public record CreateUserDto (
    @NotBlank(message = "Email address is required")
    @Email(message = "This is not email address")
    @UniqueEmail
    String email,

    @NotBlank(message = "Password is required")
    String password
){}
